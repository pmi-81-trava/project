import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';


/*
  *ref - ссылка
  *value - надпись на кнопке
  *class - класс кнопки
*/
class LinkButton extends React.Component {
  render() {
    return(
      <Link to={this.props.ref}>
        <div className={"link-button " + this.props.class}>
          {this.props.value}
        </div>
      </Link>
    );
  }
}


export default LinkButton;