import React from 'react';
import LinkButton from './LinkButton.jsx';
import './style/Auth.css';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';


class Auth extends React.Component {
  render() {
    return(
      <div className="auth">
        <div className="auth__heroimg">
          <div className="auth__logo">
            <h1>TPAVA</h1>
            <div className="auth__line"></div>
          </div>
        </div>
        <div className="auth__info">
          <div className="auth__auth-reg">
            <LinkButton value="ВХОД" class="auth__enter" />
            <LinkButton value="РЕГИСТРАЦИЯ" class="auth__registration" />
          </div>
          <div className="auth__about">
            <h3>О НАС</h3>
            <h4>Что такое TPAVA?</h4>
            <ul>
              <li>- ваш личный <span className="orange">помощник</span> в уходе за растениями;</li>
              <li>- <span className="orange">органайзер</span>. Теперь всё самое необходимое в одном месте;</li>
              <li>
                - <span className="orange">мотиватор</span>. Ничто так не вдохновляет, как собственные достижения.
                Наше приложение не упустит ни один ваш результат,
                а напоминания о них могут стать еще одним поводом похвалить себя.;
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}


export default Auth;