import React from 'react';
import ReactDOM from 'react-dom';
import './style/index.css';
import Auth from './Auth';
import { createBrowserHistory } from 'history';
import { Switch, Route, Redirect, Router } from 'react-router-dom';

const history = createBrowserHistory();

class App extends React.Component {
  render() {
    const { history } = this.props;

    return (
      <div className="App">
        <Switch>
          <Route history={history} path='/auth' component={Auth} />
          <Redirect from='/' to='/auth'/>
        </Switch>
      </div>
    );
  }
}

ReactDOM.render((
  <Router history={history}>
    <App />
  </Router>
  ),
  document.getElementById('root')
);