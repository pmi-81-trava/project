import setuptools
setuptools.setup(name='trava',
                 version='0.1.0',
                 package_dir={'': 'src'},
                 packages=setuptools.find_packages(where='src'),
                 python_requires='>=3.6, <4',
                 include_package_data=True,
                 install_requires=[
                     'flask>=1.1,<2.0',
                     'flask-cors>=3.0,<4.0',
                 ])
