import os

from flask import Flask
from flask_cors import CORS
from werkzeug.exceptions import HTTPException

from trava import db, auth, hello


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    CORS(app)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'trava.sqlite3'),
    )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.update(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.errorhandler(HTTPException)
    def handle_exception(e):
        response = e.get_response()
        response.data = e.description
        response.content_type = 'text/plain'
        return response

    db.init_app(app)

    app.register_blueprint(auth.bp)
    app.register_blueprint(hello.bp)

    return app
