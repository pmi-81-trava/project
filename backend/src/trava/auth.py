import random
import sqlite3
import functools
from collections import defaultdict

from flask import Blueprint, g, request, jsonify
from werkzeug.security import check_password_hash, generate_password_hash
from werkzeug.exceptions import BadRequest, Conflict, Unauthorized

from trava import v
from trava.db import get_db

bp = Blueprint('auth', __name__, url_prefix='/auth')


def generate_token(con, user_id):
    token = ''.join(random.choice('0123456789abcdef') for _ in range(32))
    con.execute('INSERT INTO token VALUES (?,?)', (token, user_id))

    return token


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        token = request.headers.get('token')

        if token is None:
            raise BadRequest('HTTP Header must contain a `Token` field')
        else:
            result = get_db().execute(
                'SELECT user_id FROM token WHERE token=?',
                (token, )).fetchone()

            if result is None:
                raise Unauthorized('Invalid token')
            else:
                g.user_id = result[0]

        return view(**kwargs)

    return wrapped_view


@bp.route('/register', methods=('POST', ))
def register():
    body = v.ValidatedBody(
        first_name=v.Required(v.String(min=2)),
        last_name=v.Required(v.String(min=2)),
        middle_name=v.Optional(v.String(min=2)),
        birthday=v.Required(v.Date()),
        email=v.Required(v.Email()),
        password=v.Required(v.String(min=1)),
    )

    password_hash = generate_password_hash(body.password)

    with get_db() as con:
        try:
            user_id = con.execute(
                'INSERT INTO user ('
                '    first_name, last_name, middle_name,'
                '    birthday, email, password_hash'
                ') VALUES (?,?,?,?,?,?)',
                (body.first_name, body.last_name, body.middle_name,
                 body.birthday, body.email, password_hash)).lastrowid
        except sqlite3.IntegrityError:
            raise Conflict('User with this email already exists')

        token = generate_token(con, user_id)

    return jsonify(token=token), 201


@bp.route('/token', methods=('POST', ))
def token_post():
    body = v.ValidatedBody(
        email=v.Required(v.Email()),
        password=v.Required(v.String(min=1)),
    )

    with get_db() as con:
        try:
            user_id, password_hash = con.execute(
                'SELECT id, password_hash FROM user WHERE email=?',
                (body.email, )).fetchone()
        except TypeError:
            raise Unauthorized('Invalid username or password')

        if check_password_hash(password_hash, body.password):
            return jsonify(token=generate_token(con, user_id))
        else:
            raise Unauthorized('Invalid username or password')


@bp.route('/token', methods=('DELETE', ))
def logout():
    token = request.headers.get('token')

    if token is None:
        raise BadRequest('HTTP Header must contain a `Token` field')
    else:
        with get_db() as con:
            con.execute('DELETE FROM token WHERE token=?', (token, ))

        return '', 204
