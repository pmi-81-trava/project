import re
import datetime

from flask import request
from werkzeug.exceptions import BadRequest


class ValidationError(Exception):
    pass


class ValidatedBody:
    def __init__(self, **fields):
        if not request.is_json:
            raise BadRequest(
                'HTTP Header `Content-Type` must be `application/json`')

        raw = request.json

        errors = []

        for name, validator in fields.items():
            try:
                validator.validate(raw.get(name))
                self.__setattr__(name, raw.get(name))
            except ValidationError as e:
                errors.append(f'Field `{name}` is invalid: {e.args[0]}')

        if errors:
            raise BadRequest('\n'.join(errors))


class Required:
    def __init__(self, arg):
        self.arg = arg

    def validate(self, input):
        if input is None or hasattr(input, '__len__') and len(input) == 0:
            raise ValidationError('field is required')
        else:
            self.arg.validate(input)


class Optional:
    def __init__(self, arg):
        self.arg = arg

    def validate(self, input):
        if input is not None and (not hasattr(input, '__len__')
                                  or len(input) != 0):
            self.arg.validate(input)


class String:
    def __init__(self, *, min=None, max=None):
        self.min = min
        self.max = max

    def validate(self, input):
        if not isinstance(input, str):
            raise ValidationError(f'expected str, got {type(input).__name__}')
        if self.min is not None and len(input) < self.min:
            raise ValidationError(f'string is too short, min = {self.min}')
        if self.max is not None and len(input) > self.max:
            raise ValidationError(f'string is too long, max = {self.max}')


class Date:
    def validate(self, input):
        String(min=8, max=10).validate(input)

        try:
            datetime.datetime.strptime(input, '%Y-%m-%d')
        except ValueError as e:
            raise ValidationError(e.args[0])


class Email:
    # https://html.spec.whatwg.org/#email-state-(type=email)
    RE = re.compile(r"^"
                    r"[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+"
                    r"@"
                    r"[a-zA-Z0-9]"
                    r"(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?"
                    r"(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+"
                    r"$")

    def validate(self, input):
        String(min=5).validate(input)

        if self.RE.match(input) is None:
            raise ValidationError('not a valid email')
