-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS token;
DROP TABLE IF EXISTS user;

CREATE TABLE token (
    token      TEXT    PRIMARY KEY,
    user_id    INTEGER NOT NULL REFERENCES user(id)
);
CREATE INDEX token_user_id ON token(user_id);

CREATE TABLE user (
    id            INTEGER PRIMARY KEY AUTOINCREMENT,
    email         TEXT    NOT NULL UNIQUE,
    password_hash TEXT    NOT NULL,
    first_name    TEXT    NOT NULL,
    last_name     TEXT    NOT NULL,
    middle_name   TEXT,
    birthday      TEXT
);
