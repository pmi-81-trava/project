from flask import Blueprint, g

from trava.auth import login_required

bp = Blueprint('hello', __name__, url_prefix='/hello')


@bp.route('/guest', methods=('GET', ))
def guest():
    return 'Hello, guest!'


@bp.route('/user', methods=('GET', ))
@login_required
def user():
    return f'Hello, user #{g.user_id}!'
