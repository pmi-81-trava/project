# TPAVA Flask backend

## Установка и запуск для разработки

### Linux

Полная установка и запуск
```bash
# Скачать проект
git clone https://gitlab.com/pmi-81-trava/project
cd project/backend
# Подготовить виртуальное окружение, чтобы не засорять систему
python3 -m venv venv
source venv/bin/activate
# Установка зависимостей
venv/bin/pip3 install -e .
# Необходимые переменные окружения
export FLASK_APP=trava
export FLASK_ENV=development
# Первоначальная настройка базы данных
venv/bin/flask init-db
# Запуск сервера
venv/bin/flask run
# Для остановки сервера нажмите ctrl-c
# Выход из виртуального окружения
deactivate
```

Только запуск
```bash
# Вход в виртуальное окружение
cd project/backend
source venv/bin/activate

export FLASK_APP=trava
export FLASK_ENV=development
venv/bin/flask run
# Для остановки сервера нажмите ctrl-c

# Выход из виртуального окружения
deactivate
```
